import unittest
import calculator as cal
from platform import system


class TestMathsMethods(unittest.TestCase):
    def test_add_positive(self):
        self.assertEqual(cal.Logic.add(cal.dec.Decimal('1'),
                                       cal.dec.Decimal('3')),
                         cal.dec.Decimal('4'))

    def test_add_negative(self):
        self.assertEqual(cal.Logic.add(cal.dec.Decimal('1'),
                                       cal.dec.Decimal('-3')),
                         cal.dec.Decimal('-2'))

    def test_add_decimal(self):
        self.assertEqual(cal.Logic.add(cal.dec.Decimal('0.45'),
                                       cal.dec.Decimal('2.99')),
                         cal.dec.Decimal('3.44'))

    def test_subtract_positive(self):
        self.assertEqual(cal.Logic.subtract(cal.dec.Decimal('4'),
                                            cal.dec.Decimal('3')),
                         cal.dec.Decimal('1'))

    def test_subtract_negative(self):
        self.assertEqual(cal.Logic.subtract(cal.dec.Decimal('1'),
                                            cal.dec.Decimal('-3')),
                         cal.dec.Decimal('4'))

    def test_subtract_decimal(self):
        self.assertEqual(cal.Logic.subtract(cal.dec.Decimal('0.45'),
                                            cal.dec.Decimal('2.99')),
                         cal.dec.Decimal('-2.54'))

    def test_multiply_positive(self):
        self.assertEqual(cal.Logic.multiply(cal.dec.Decimal('2'),
                                            cal.dec.Decimal('3')),
                         cal.dec.Decimal('6'))

    def test_multiply_negative(self):
        self.assertEqual(cal.Logic.multiply(cal.dec.Decimal('2'),
                                            cal.dec.Decimal('-3')),
                         cal.dec.Decimal('-6'))

    def test_multiply_decimal(self):
        self.assertEqual(cal.Logic.multiply(cal.dec.Decimal('0.45'),
                                            cal.dec.Decimal('2.99')),
                         cal.dec.Decimal('1.3455'))

    def test_divide_positive(self):
        self.assertEqual(cal.Logic.divide(cal.dec.Decimal('6'),
                                          cal.dec.Decimal('3')),
                         cal.dec.Decimal('2'))

    def test_divide_negative(self):
        self.assertEqual(cal.Logic.divide(cal.dec.Decimal('-6'),
                                          cal.dec.Decimal('3')),
                         cal.dec.Decimal('-2'))

    def test_divide_decimal(self):
        self.assertEqual(cal.Logic.divide(cal.dec.Decimal('3'),
                                          cal.dec.Decimal('1.5')),
                         cal.dec.Decimal('2'))


@unittest.skipIf(system() == 'Linux', 'Running on Linux')
# Used to prevent execution of GUI based tests on BitBucket Pipeline.
class TestGUIMethods(unittest.TestCase):
    def setUp(self):
        self.btnFrm = cal.ButtonFrame(cal.tk.Tk())
        self.button_character_list = ['0', '1', '2', '3', '4', '5', '6', '7',
                                      '8', '9', '+', '-', 'x', '/', '.']
        self.char = self.button_character_list[0]

    def tearDown(self):
        self.btnFrm.destroy()

    def test_button_text(self):
        btns = self.btnFrm.generate_buttons(self.button_character_list)
        self.assertEqual(btns[0]['text'], self.char)


class TestValidationMethods(unittest.TestCase):
    def setUp(self):
        cal.Logic.solved = False

    def test_validation_invalid(self):
        self.assertFalse(cal.Logic.validate('a', ''))

    # Test that the entry of operands works
    def test_validation_first_term(self):
        self.assertTrue(cal.Logic.validate('4', ''))

    def test_validation_second_term(self):
        self.assertTrue(cal.Logic.validate('4', '3+4'))

    # Test that the entry of operators work
    def test_validation_operator_add(self):
        self.assertTrue(cal.Logic.validate('+', '5+'))

    def test_validation_operator_subtract(self):
        self.assertTrue(cal.Logic.validate('-', '5-'))

    def test_validation_operator_multiply(self):
        self.assertTrue(cal.Logic.validate('x', '5x'))

    def test_validation_operator_divide(self):
        self.assertTrue(cal.Logic.validate('/', '5/'))

    # Test that two operators can't be entered
    def test_validation_operator_double(self):
        self.assertFalse(cal.Logic.validate('/', '2+/'))

    # But equals can
    def test_validation_operator_adds_equals(self):
        self.assertTrue(cal.Logic.validate('=', '4+3='))

    def test_validation_operator_subtract_equals(self):
        self.assertTrue(cal.Logic.validate('=', '4-3='))

    def test_validation_operator_multiply_equals(self):
        self.assertTrue(cal.Logic.validate('=', '4x3='))

    def test_validation_operator_divide_equals(self):
        self.assertTrue(cal.Logic.validate('=', '4/3='))

    # Prevent = being entered until there are two terms
    def test_validation_operator_equals_single_term(self):
        self.assertFalse(cal.Logic.validate('=', '4='))

    def test_validation_operator_equals_single_term_and_operator(self):
        self.assertFalse(cal.Logic.validate('=', '4+='))

    # Test aspects of entering decimals
    def test_validation_decimal_one_term(self):
        self.assertTrue(cal.Logic.validate('.', '4.'))

    def test_validation_decimal_two_terms(self):
        self.assertTrue(cal.Logic.validate('.', '4.0+3.'))

    def test_validation_decimal_double_second_term(self):
        self.assertFalse(cal.Logic.validate('.', '4.0+3..'))

    def test_validation_decimal_double_first_term(self):
        self.assertFalse(cal.Logic.validate('.', '4..'))

    def test_validation_decimal_double_split(self):
        self.assertFalse(cal.Logic.validate('.', '4.0+3.2.'))

    def test_validation_decimal_after_dot(self):
        self.assertFalse(cal.Logic.validate('+', '4.+'))

    def test_validation_decimal_single(self):
        self.assertFalse(cal.Logic.validate('.', '.'))


@unittest.skipIf(system() == 'Linux', 'Running on Linux')
class TestSolveMethods(unittest.TestCase):
    def setUp(self):
        root = cal.tk.Tk()
        cal.gui = cal.Interface(root)
        cal.Logic.solved = False

    def test_solve_clear_wo_equals(self):
        cal.gui.input_str.set('4+Clr')
        self.assertEqual(cal.gui.input_str.get(), '')

    def test_solve_clear_w_equals(self):
        cal.gui.input_str.set('4+4=8Clr')
        self.assertEqual(cal.gui.input_str.get(), '')

    def test_solve_add(self):
        cal.gui.input_str.set('4+4=')
        self.assertEqual(cal.gui.input_str.get(), '4+4=8')

    def test_solve_subtract(self):
        cal.gui.input_str.set('4-4=')
        self.assertEqual(cal.gui.input_str.get(), '4-4=0')

    def test_solve_multiply(self):
        cal.gui.input_str.set('4x4=')
        self.assertEqual(cal.gui.input_str.get(), '4x4=16')

    def test_solve_divide(self):
        cal.gui.input_str.set('4/4=')
        self.assertEqual(cal.gui.input_str.get(), '4/4=1')

    def tearDown(self):
        del cal.gui


if __name__ == '__main__':
    unittest.main()
