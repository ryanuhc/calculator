# Import modules
import decimal as dec
import tkinter as tk
from tkinter import messagebox as mb
# Wildcard (*) imports of tkinter are used in much of the teaching material
# but can cause problems with namespace conflicts.

# Strings that can be input and are used as button text
VALUE_CHAR_LIST = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '0', '.']
CMD_CHAR_LIST = ['+', '-', 'x', '/', '=', 'Clr']
OTHER_CHAR_LIST = ['Info']
# About information for the program
INFO_TXT = {'title': 'Info', 'msg': 'Program created by Ryan Cox'}


class Logic:
    """Contains the validation and arthemitic logic.

    Store no data and is not designed to be used as an instance - none of the
    methods rely on instances of the Logic object. As such, no instances are
    created - this would have been unnecessarily complex. Instead two
    decorators are used, @staticmethod and @classmethod. @staticmethod allows
    you to call the method with Logic.methodname() and passes no class related
    parameters to the method. @classmethod also allows the method to be called
    as Logic.methodname() but passes the class as the first parameters. This is
    in constrast to normal methods, which are called with
    instancename.methodname() and are passed the instance as their first
    parameter.

    :method add: Adds 2 numbers
    :method subtract: Subtracts 2 numbers
    :method multiply: Multiply 2 numbers
    :method divide: Divide 2 numbers
    :method validate: Check if input is valid
    :method solve: Mathematically evaluate the input
    """
    solved = False

    @staticmethod
    def add(a, b):
        """Add two numbers."""
        return a + b

    @staticmethod
    def subtract(a, b):
        """Subtract two numbers."""
        return a - b

    @staticmethod
    def multiply(a, b):
        """Multiply two numbers."""
        return a * b

    @staticmethod
    def divide(a, b):
        """Divide two numbers."""
        return a / b

    @staticmethod
    def validate(char, string):
        """Validates input.

        Input char must be in VALUE_CHAR_LIST or CMD_CHAR_LIST or
        OTHER_CHAR_LIST. Returns False if input is invalid, else returns True.
        Validation rules can be easily changed, such as when the program is
        expanded by adding the characters to the appropriate lists and
        modifying the below logic so that it returns True or False as
        appropriate for the possible input.
        """
        # Only allow certain characters
        if char not in (VALUE_CHAR_LIST + CMD_CHAR_LIST) and char != '':
            return False
        # Do not allow repeated operators
        elif 'Clr' == char:
            return True
        # Prevent stuff being added to solved equations
        elif Logic.solved and '=' in string:
            return False
        # Handle operators
        elif char in CMD_CHAR_LIST:
            i = 0
            for symbol in string:
                if symbol in CMD_CHAR_LIST and char != '=':
                    i += 1
                    if i > 1:
                        return False
            # Prevent starting with operator
            if string == char:
                return False
            # Require a decimal point to be followed by a digit
            elif string[-2] == '.':
                return False
            elif char == '=':
                # Prevent entering = directly after operator
                if string[-2] in CMD_CHAR_LIST:
                    return False
                else:
                    # Prevent entering = in equation without operator
                    for cmd in CMD_CHAR_LIST[:-2]:
                        if cmd in string:
                            return True
                    return False

        # Do not allow starting with a decimal point
        elif string == '.':
            return False
        # Do not allow mupltiple decimal points in a term
        safe_dec = False
        for cmd in CMD_CHAR_LIST:
            if cmd in string:
                terms = string.split(cmd)
                for term in terms:
                    if term.count('.') > 1:
                        return False
                    else:
                        safe_dec = True
        else:
            if string.count('.') > 1 and not safe_dec:
                return False
        return True

    @classmethod
    def solve(parent_class, *args):
        """Process input and carries out commands.

        If the input contains = it returns the solution and resets style.
        Elif input contains Clr or is empty it clears everything and resets
        style.
        Elif input contains an operator it styles it.

        :param parent_class: The encapsulating class (Logic)
        :param gui: The Interface object.
        :param *args: tkinter includes arguments with the callback that we need
        to accept.
        """
        mode = ''
        string = gui.input_str.get()
        # Handle clearing and cleared strings
        if 'Clr' in string or gui.input_str == '':
            gui.input_str.set('')
            gui.buttonsFrm.unmark()
            return
        elif not Logic.validate(string[-1], string):
            gui.input_str.set(string[:-1])
            return
        # Solve input
        elif '=' in string:
            string = string.strip('=')
            mode = ''
            for char in CMD_CHAR_LIST:
                if char in string:
                    mode = char
                    break
            term1, term2 = string.split(mode)
            # Convert to Decimal object for accuracy with floating point
            # numbers.
            try:
                term1 = dec.Decimal(term1)
                term2 = dec.Decimal(term2)
            except dec.InvalidOperation:
                term1 = 0
                term2 = 0
            if mode == '+':
                solution = parent_class.add(term1, term2)
            elif mode == '-':
                solution = parent_class.subtract(term1, term2)
            elif mode == 'x':
                solution = parent_class.multiply(term1, term2)
            elif mode == '/':
                solution = parent_class.divide(term1, term2)
            gui.input_str.set(string + '=' + str(solution))
            gui.buttonsFrm.unmark()
            Logic.solved = True
            return
        # Mark selected operand
        else:
            Logic.solved = False
            for char in string:
                if char in CMD_CHAR_LIST:
                    gui.buttonsFrm.mark(char)
                    return
                else:
                    gui.buttonsFrm.unmark()
                    return


class Interface:

    """Creates and manages the GUI components.

    :var input_str: The current input from buttons and/or the textbox.
    :method __init__: Class constructor
    """

    def __init__(self, parent):
        """Create GUI frames and initalise interface wide variables."""
        self.parent = parent
        self.input_str = tk.StringVar()
        self.textboxFrm = TextboxFrame(self)
        self.buttonsFrm = ButtonFrame(self)
        # Call solve whenever input_str changed
        self.input_str.trace_add('write', lambda *args: Logic.solve(self))


class TextboxFrame(tk.Frame):

    """Frame that includes textbox for keyboard entry and input display.

    :var parent: Parent tk object known as gui
    :var textbox: tk.Entry object
    :method __init__: Class constructor
    :method on_entry: Character entry handling
    """

    def __init__(self, parent):
        """Construct TextboxFrame"""
        super(TextboxFrame, self).__init__()
        self.parent = parent
        self.pack()
        on_entry = self.register(self.on_entry)
        self.textbox = tk.Entry(self, textvariable=self.parent.input_str,
                                width=40,
                                validate='key',
                                validatecommand=(on_entry, '%S', '%P'))
        self.textbox.pack(ipady=10)

    def on_entry(self, char, string):
        """Calls input validation.

        This method exists (rather than just calling Logic.validate() with
        the validatecommand parameter of self.textbox) so that parameters can
        be passed to Logic.validate() and so that the method can be registered.

        :return True: Validated as valid and added to string.
        :return False: Validated as invalid. Prevents entry.
        """
        if not Logic.validate(char, string):
            return False
        return True


class ButtonFrame(tk.Frame):

    """Frame that includes all the buttons for user input.

    :var parent: Parent tk object known as gui
    :var button_character_list: All the symbols used for buttons.
    :var btns: A list of tk.Button objects
    :method __init__: Class constructor
    :method generate_buttons: Turn character list into tk.Button objects
    :method display_buttons: Display buttons from list
    :method add_character: Proccess and add selected char to input_str
    :method mark: Style relief of chosen button.
    :method unmark: Style relief of all buttons to return to inital state.
    """

    def __init__(self, parent):
        """Class constructor"""
        super(ButtonFrame, self).__init__()
        self.parent = parent
        self.pack()
        self.button_character_list = VALUE_CHAR_LIST + CMD_CHAR_LIST +\
            OTHER_CHAR_LIST
        self.btns = self.generate_buttons(self.button_character_list)
        self.display_buttons(self.btns, 3, 6)

    def generate_buttons(self, btn_list):
        """Take list of characters (as strings) and turn them into tk.Button
        objects."""
        btns = []
        for char in btn_list:
            btns.append(tk.Button(self, text=char,
                                  command=lambda char=char:
                                  self.add_character(char),
                                  height=3,
                                  width=10,
                                  bg='#ccc' if char in VALUE_CHAR_LIST
                                     else '#fff'))
        return btns

    def display_buttons(self, btns, width, height):
        """Arrange tk.Button objects from a list into a grid."""
        index = 0
        for rowvar in range(height):
            for columnvar in range(width):
                btns[index].grid(row=rowvar, column=columnvar)
                index += 1
                if index == len(btns):
                    break

    def add_character(self, char):
        """Add a character to the current input or display info box"""
        string = self.parent.input_str.get()
        if char == 'Info':
            # This box is badly placed due to limitations of the function used
            mb.showinfo(title=INFO_TXT['title'], message=INFO_TXT['msg'])
        elif Logic.validate(char, string + char):
            self.parent.input_str.set(string + char)

    def mark(self, char):
        """Change button styling to indicate selection."""
        for button in self.btns:
            if char == button['text']:
                button['relief'] = 'sunken'
                break

    def unmark(self):
        """Style all buttons as per default, reversing mark()"""
        for button in self.btns:
            button['relief'] = 'raised'


if __name__ == '__main__':
    """Run GUI."""
    root = tk.Tk()
    gui = Interface(root)
    root.title('Calculator')
    root.mainloop()
